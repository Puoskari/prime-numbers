import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

public class PrimeNumbers {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("File name needs to be provided as an entry parameter.");
        } else if (args.length > 1) {
            System.out.println("Only one parameter is allowed.");
        } else {
            System.out.println("Prime numbers from the file:");
            loadExcelFile(args[0]);
        }
    }

    private static void loadExcelFile(String fileName) {
        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file);

            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet sheet = wb.getSheetAt(0);

            for (Row row : sheet) {
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    if (cell.getCellType() == CellType.STRING && isPrimeNumber(cell.getStringCellValue())) {
                        System.out.println(cell.getStringCellValue());
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean isPrimeNumber(String cellContent) {
        try {
            float number = Float.parseFloat(cellContent);
            if (number < 2 || number % 1 != 0) {
                return false;
            } else {
                for (int i = 2; i <= number / 2; ++i) {
                    if (number % i == 0) {
                        return false;
                    }
                }
            }
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
}
